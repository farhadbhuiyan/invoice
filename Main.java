public class Main{
	public static void main (String[] args){
		
		/*Customer customer1 = new Customer();
		
		customer1.setCustomerId(1101);
		customer1.setCustomerName("Mr. Xyz");*/
		
		Product[] product = new Product[2];
		
		product[0] = new Product(101, "Mobile", 2000, 1101); 
		product[1] = new Product(102, "Camera", 3000, 1102);

		//create customer with ID 1101
		Customer customer1 = new Customer();
		
		customer1.setCustomerId(1101);
		customer1.setCustomerName("Mr Xyz");
		customer1.getInvoice().setQuantity(2);
		
		customer1.print();
		
		
	}
}