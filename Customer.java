public class Customer{
	
	private int customerId;
	private String customerName;
	private Invoice invoice;
	
	Customer(){
		
		this.invoice = new Invoice();
	}
	
	public void setCustomerId(int customerId){
		
		this.customerId = customerId;
	}
	
	public int getCustomerId(){
		
		return this.customerId;
	}
	
	public void setCustomerName(String customerName){
		
		this.customerName = customerName;
	}
	
	public String getcustomerName(){
		
		return this.customerName;
	}
	
	public Invoice getInvoice(){
		
		return this.invoice;
	}
	
	public void print(){
		System.out.println("Customer Id: " + this.customerId);
		System.out.println("Customer Name: " + this.customerName);
		System.out.println("Quantity: " + this.invoice.getQuantity());
		System.out.println("Grand Total:" + this.invoice.getGrandTotal());
	}
}