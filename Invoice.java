public class Invoice{
	
	private int itemNo;
	private int quantity;
	private Product product;
	private int grandTotal;
	
	
	Invoice(){
		
		this.product = new Product();
	}
	
	public void setQuantity(int quantity){
		
		this.quantity = quantity;
	}
	
	public int getQuantity(){
		
		return this.quantity;
	}
	
	public void setGrandTotal(){
		
		this.grandTotal = this.product.getProductPrice() * this.quantity;
	}
	
	public int getGrandTotal(){
		return this.grandTotal;
	}
	
}